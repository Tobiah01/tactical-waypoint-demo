#include <map>

#include "UnitManager.h"
#include "Unit.h"
#include "Game.h"
#include "ComponentManager.h"
#include "GraphicsSystem.h"
#include "DataKeeper.h"
#include "SpriteManager.h"
#include "Sprite.h"

UnitID UnitManager::msNextUnitID = PLAYER_UNIT_ID + 1;

using namespace std;

UnitManager::UnitManager(Uint32 maxSize)
	:mPool(maxSize, sizeof(Unit))
{
}

Unit* UnitManager::createUnit(const Sprite& sprite, bool isPowerUp, bool shouldWrap, const PositionData& posData /*= ZERO_POSITION_DATA*/, const PhysicsData& physicsData /*= ZERO_PHYSICS_DATA*/, const UnitID& id)
{
	Unit* pUnit = NULL;

	Byte* ptr = mPool.allocateObject();
	if (ptr != NULL)
	{
		//create unit
		pUnit = new (ptr)Unit(sprite);//placement new

		UnitID theID = id;
		if (theID == INVALID_UNIT_ID)
		{
			theID = msNextUnitID;
			msNextUnitID++;
		}

		if (!isPowerUp) 
		{
			//place in map
			pUnit->setWaypoint(false);
			mUnitMap[theID] = pUnit;
		}
		else if (isPowerUp) 
		{
			pUnit->setWaypoint(true);
			mWayPoints[theID] = pUnit;
		}
		//assign id and increment nextID counter
		pUnit->mID = theID;

		//create some components
		ComponentManager* pComponentManager = gpGame->getComponentManager();
		ComponentID id = pComponentManager->allocatePositionComponent(posData,shouldWrap);
		pUnit->mPositionComponentID = id;
		pUnit->mpPositionComponent = pComponentManager->getPositionComponent(id);
		pUnit->mPhysicsComponentID = pComponentManager->allocatePhysicsComponent(pUnit->mPositionComponentID, physicsData);
		pUnit->mSteeringComponentID = pComponentManager->allocateSteeringComponent(pUnit->mPhysicsComponentID);

		//set max's
		pUnit->mMaxSpeed = MAX_SPEED;
		pUnit->mMaxAcc = MAX_ACC;
		pUnit->mMaxRotAcc = MAX_ROT_ACC;
		pUnit->mMaxRotVel = MAX_ROT_VEL;
		pUnit->createStateMechine();
	}

	return pUnit;
}


Unit* UnitManager::createPlayerUnit(const Sprite& sprite, bool shouldWrap /*= true*/, const PositionData& posData /*= ZERO_POSITION_DATA*/, const PhysicsData& physicsData /*= ZERO_PHYSICS_DATA*/)
{
	return createUnit(sprite, false, shouldWrap, posData, physicsData, PLAYER_UNIT_ID);
}


Unit* UnitManager::createRandomUnit(const Sprite& sprite)
{

	int posX = gpGame->getGraphicsSystem()->getWidth();
	int posY = gpGame->getGraphicsSystem()->getHeight();

	Unit* pUnit = createUnit(sprite, false, true, PositionData(PositionData(Vector2D(posX, posY), 0)));
	if (pUnit != NULL)
	{

		pUnit->setSteering(Steering::WANDER_STEERING, Vector2D(posX, posY), INVALID_UNIT_ID);
		pUnit->mStats.damage = gpGame->getDataKeeper()->getDataFile().enemyDamage;
		pUnit->mStats.health = gpGame->getDataKeeper()->getDataFile().enemyHealth;
		pUnit->mStats.damageRadius = gpGame->getDataKeeper()->getDataFile().enemyDamageRaduis;
		pUnit->mStats.collistionRadius = 0.0f;
		pUnit->mStats.sitghtRange = gpGame->getDataKeeper()->getDataFile().enemySightRange;
		pUnit->mStats.speedPercent = gpGame->getDataKeeper()->getDataFile().enemySpeedPercent;
	}
	return pUnit;
}

Unit* UnitManager::createRandomPowerUp(const Sprite& sprite)
{
	int posX = rand() % gpGame->getGraphicsSystem()->getWidth();
	int posY = rand() % gpGame->getGraphicsSystem()->getHeight();
	int velX = rand() % 50 - 25;
	int velY = rand() % 40 - 20;

	int type = (int) rand() % 3;

	Unit* pUnit = createUnit(sprite, true, true, PositionData(PositionData(Vector2D(posX, posY), 0)));;
	if (pUnit != NULL)
	{
		pUnit->mStats.damage = 0.0f;//getSates().mDamage = 0.0f;
		pUnit->mStats.health = 0.0f;
		pUnit->mStats.damageRadius = 0.0f;
		pUnit->mStats.collistionRadius = gpGame->getDataKeeper()->getDataFile().powerUpCollistionRadius;
		pUnit->setPowerUpType(powerType(type));
		pUnit->mStats.speedPercent = 0.0f;
	}
	return pUnit;
}

Unit* UnitManager::createWaypoint(const Sprite& sprite, Vector2D pos)
{
	

	int type = (int)rand() % 3;

	Unit* pUnit = createUnit(sprite, true, true, PositionData(PositionData(Vector2D(pos.getX(), pos.getY()), 0)));;
	if (pUnit != NULL)
	{
		pUnit->mStats.damage = 0.0f;//getSates().mDamage = 0.0f;
		pUnit->mStats.health = 0.0f;
		pUnit->mStats.damageRadius = gpGame->getDataKeeper()->getDataFile().enemyDamageRaduis;
		pUnit->mStats.collistionRadius = 0.0f;
		pUnit->mStats.speedPercent = 0.0f;
	}
	return pUnit;
}

Unit* UnitManager::getUnit(const UnitID& id) const
{
	auto it = mUnitMap.find(id);
	if (it != mUnitMap.end())//found?
	{
		return it->second;
	}
	else
	{
		return NULL;
	}
}

Unit* UnitManager::getWaypoint(const UnitID& id) const
{
	auto it = mWayPoints.find(id);
	if (it != mWayPoints.end())//found?
	{
		return it->second;
	}
	else
	{
		return NULL;
	}
}

void UnitManager::deleteUnit(const UnitID& id)
{
	auto it = mUnitMap.find(id);
	if (it != mUnitMap.end())//found?
	{
		Unit* pUnit = it->second;//hold for later

		//remove from map
		mUnitMap.erase(it);

		//remove components
		ComponentManager* pComponentManager = gpGame->getComponentManager();
		pComponentManager->deallocatePhysicsComponent(pUnit->mPhysicsComponentID);
		pComponentManager->deallocatePositionComponent(pUnit->mPositionComponentID);
		pComponentManager->deallocateSteeringComponent(pUnit->mSteeringComponentID);

		//call destructor
		pUnit->~Unit();

		//free the object in the pool
		mPool.freeObject((Byte*)pUnit);
	}
}

void UnitManager::deletePowerUp(const UnitID& id)
{

	auto it = mPowerUpMap.find(id);
	if (it != mPowerUpMap.end())//found?
	{
		Unit* pUnit = it->second;//hold for later

								 //remove from map
		mPowerUpMap.erase(it);

		//remove components
		ComponentManager* pComponentManager = gpGame->getComponentManager();
		pComponentManager->deallocatePhysicsComponent(pUnit->mPhysicsComponentID);
		pComponentManager->deallocatePositionComponent(pUnit->mPositionComponentID);
		pComponentManager->deallocateSteeringComponent(pUnit->mSteeringComponentID);

		//call destructor
		pUnit->~Unit();

		//free the object in the pool
		mPool.freeObject((Byte*)pUnit);
	}



}

void UnitManager::deleteRandomUnit()
{
	if (mUnitMap.size() >= 1)
	{
		Uint32 target = rand() % mUnitMap.size();
		if (target == 0)//don't allow the 0th element to be deleted as it is the player unit
		{
			target = 1;
		}
		Uint32 cnt = 0;
		for (auto it = mUnitMap.begin(); it != mUnitMap.end(); ++it, cnt++)
		{
			if (cnt == target)
			{
				deleteUnit(it->first);
				break;
			}
		}
	}
}

void UnitManager::drawAll() const
{
	for (auto it = mUnitMap.begin(); it != mUnitMap.end(); ++it)
	{
		it->second->draw();
	}

	for (auto it =  mWayPoints.begin(); it != mWayPoints.end(); ++it)
	{
		it->second->draw();
	}
}

void UnitManager::updateAll(float elapsedTime)
{
	int count = 0;

	for (auto it = mUnitMap.begin(); it != mUnitMap.end(); ++it)
	{
		checkHealth(it->first);
		checkEnemyVSPlayerRadius(it->first);
		checkPlayerVSEnemtRadius(it->first);
		pickUpPower(it->first);
		it->second->update(elapsedTime);
		count++;
	}

	if (count < 4) 
	{
		attackState = false;
	}

	if (count >= 10) 
	{
		attackState = true;
	}

	for (auto it = mWayPoints.begin(); it != mWayPoints.end(); ++it)
	{
		it->second->update(elapsedTime);
		it->second->setWaypointValue(calcWaypointValue(it->first));
		it->second->update(elapsedTime);
	}

	getClosestWaypoint();

	spawnUnits();

	deleteEnmeies();



}

void UnitManager::deleteEnmeies() 
{
	for (int g = 0; g < removeUnits.size(); g++) 
	{
		deleteUnit(removeUnits[g]);
		gpGame->setPlayerScore(gpGame->getPlayerScore() + 100.0);

	}

	for (int g = 0; g < removePowerUps.size(); g++)
	{
		deletePowerUp(removePowerUps[g]);

	}

	removeUnits.clear();
	removePowerUps.clear();
}

void UnitManager::checkHealth(UnitID id) 
{


	if (getUnit(id)->getStats().health <= 0.0f)
	{
		if (id != PLAYER_UNIT_ID) 
		{
			removeUnits.push_back(id);
		}
		else if (id == PLAYER_UNIT_ID) 
		{
			gpGame->setPlayerIsAlive(false);
		}
	}


}

void UnitManager::checkEnemyVSPlayerRadius(UnitID id) 
{
	if (getPlayerUnit() != getUnit(id))
	{

		
		float xPos = getUnit(id)->getPositionComponent()->getPosition().getX();
		float yPos = getUnit(id)->getPositionComponent()->getPosition().getY();

		float upperLeftX = getPlayerUnit()->mArea.upperLeftX;
		float LowerRightX = getPlayerUnit()->mArea.lowerRightX;
		
		float upperLeftY = getPlayerUnit()->mArea.upperLeftY;
		float LowerRightY = getPlayerUnit()->mArea.lowerRightY;


		float dist = getPlayerUnit()->getPositionComponent()->getPosition().getLength() - getUnit(id)->getPositionComponent()->getPosition().getLength();


		if (xPos < upperLeftX && xPos > LowerRightX)
		{
			if (yPos < upperLeftY && yPos > LowerRightY)
			{
				float tempHealth = getUnit(id)->mStats.health - getPlayerUnit()->mStats.damage;
				getUnit(id)->mStats.health = tempHealth;
			}
		}
	}
}

void UnitManager::checkPlayerVSEnemtRadius(UnitID id) 
{
	if (getPlayerUnit() != getUnit(id))
	{


		float xPos = getPlayerUnit()->getPositionComponent()->getPosition().getX();
		float yPos = getPlayerUnit()->getPositionComponent()->getPosition().getY();

		float upperLeftX = getUnit(id)->mArea.upperLeftX;
		float LowerRightX = getUnit(id)->mArea.lowerRightX;

		float upperLeftY = getUnit(id)->mArea.upperLeftY;
		float LowerRightY = getUnit(id)->mArea.lowerRightY;


		float dist = getPlayerUnit()->getPositionComponent()->getPosition().getLength() - getUnit(id)->getPositionComponent()->getPosition().getLength();
		getUnit(id)->setUpperLeftX(0);

		if (xPos < upperLeftX && xPos > LowerRightX)
		{
			if (yPos < upperLeftY && yPos > LowerRightY)
			{
				float tempHealth = getPlayerUnit()->mStats.health - getUnit(id)->mStats.damage;
				getPlayerUnit()->mStats.health = tempHealth;
			}
		}
	}
}


void UnitManager::pickUpPower(UnitID id) 
{
	if (getPlayerUnit() != getUnit(id))
	{
		for (auto it = mPowerUpMap.begin(); it != mPowerUpMap.end(); ++it)
		{
			
			float dist = it->second->getPositionComponent()->getPosition().getLength() - getUnit(id)->getPositionComponent()->getPosition().getLength();


			if (abs(dist) <= it->second->mStats.collistionRadius)
			{
				getUnit(id)->unitPowerUp(it->second->getPowerUpType());
				removePowerUps.push_back(it->first);
			}

		}
	}
}


bool UnitManager::isPickupNear(UnitID unitID) 
{

	for (auto it = mPowerUpMap.begin(); it != mPowerUpMap.end(); ++it)
	{

		float dist = it->second->getPositionComponent()->getPosition().getLength() - getUnit(unitID)->getPositionComponent()->getPosition().getLength();


		if (abs(dist) <= getUnit(unitID)->mStats.sitghtRange)
		{
			return true;
		}
	}

	return false;
}

bool UnitManager::isNearPlayer(UnitID unitID) 
{
	float dist = getPlayerUnit()->getPositionComponent()->getPosition().getLength() - getUnit(unitID)->getPositionComponent()->getPosition().getLength();


	if (abs(dist) <= getUnit(unitID)->mStats.sitghtRange)
	{
		return true;
	}

	return false;
}


Vector2D UnitManager::getNearbyPickupLoc(UnitID unitID) 
{
	for (auto it = mPowerUpMap.begin(); it != mPowerUpMap.end(); ++it)
	{

		float dist = it->second->getPositionComponent()->getPosition().getLength() - getUnit(unitID)->getPositionComponent()->getPosition().getLength();

		

		if (abs(dist) <= getUnit(unitID)->mStats.sitghtRange)
		{
			return it->second->getPositionComponent()->getPosition();
		}
	}

	return ZERO_VECTOR2D;
}


int UnitManager::calcWaypointValue(UnitID id)
{
	float waypointXPos = getWaypoint(id)->getPositionComponent()->getPosition().getX();
	float waypointYPos = getWaypoint(id)->getPositionComponent()->getPosition().getY();

	float playerXPos = getPlayerUnit()->getPositionComponent()->getPosition().getX();
	float playerYPos = getPlayerUnit()->getPositionComponent()->getPosition().getY();

	float playerUpperLeftX = getPlayerUnit()->mArea.upperLeftX;
	float playerLowerRightX = getPlayerUnit()->mArea.lowerRightX;

	float playerUpperLeftY = getPlayerUnit()->mArea.upperLeftY;
	float playerLowerRightY = getPlayerUnit()->mArea.lowerRightY;

	
	//getWaypoint(id)->mArea.lowerRightX = getWaypoint(id)->getPositionComponent()->getPosition().getX() - getWaypoint(id)->mStats.damageRadius;
	//getWaypoint(id)->mArea.lowerRightY = getWaypoint(id)->getPositionComponent()->getPosition().getY() - getWaypoint(id)->mStats.damageRadius;

	//getWaypoint(id)->mArea.upperLeftX = getWaypoint(id)->getPositionComponent()->getPosition().getX() + getWaypoint(id)->mStats.damageRadius;
	//getWaypoint(id)->mArea.upperLeftY = getWaypoint(id)->getPositionComponent()->getPosition().getY() + getWaypoint(id)->mStats.damageRadius;



	float waypointUpperLeftX = getWaypoint(id)->mArea.upperLeftX;
	float waypointLowerRightX = getWaypoint(id)->mArea.lowerRightX;

	float waypointUpperLeftY = getWaypoint(id)->mArea.upperLeftY;
	float waypointLowerRightY = getWaypoint(id)->mArea.lowerRightY;


	
	if (waypointXPos < playerUpperLeftX && waypointXPos > playerLowerRightX)
	{
		if (waypointYPos < playerUpperLeftY && waypointYPos > playerLowerRightY)
		{
			if (attackState) 
			{
				return 4;
			}


			return 0;
		}
	}


	if (playerXPos < waypointUpperLeftX && playerXPos > waypointLowerRightX)
	{
		if (playerYPos < waypointUpperLeftY && playerYPos > waypointLowerRightY)
		{

			return 3;
		}
	}

	return 1;
}

void UnitManager::getClosestWaypoint()
{
	
	float waypointXPos = 0;
	float waypointYPos = 0;
	float CloesetTotal = 10000;
	float FurthestTotal = 0;

	float playerXPos = getPlayerUnit()->getPositionComponent()->getPosition().getX();
	float playerYPos = getPlayerUnit()->getPositionComponent()->getPosition().getY();

	int idColest = 0;
	int idFurthest = 0;
	int count = 0;
	

	for (auto it = mWayPoints.begin(); it != mWayPoints.end(); ++it)
	{
		if (it->second->getWaypointValue() == 1) 
		{
			float waypointXPos = it->second->getPositionComponent()->getPosition().getX();
			float waypointYPos = it->second->getPositionComponent()->getPosition().getY();
			
			float lXPos  = abs(waypointXPos - playerXPos);
			float lYPos = abs(waypointYPos - playerYPos);

			float total = abs(lYPos + lXPos);

			if (total < CloesetTotal)
			{
				idColest = it->first;
				count++;
				CloesetTotal = total;
			}

			if (total > FurthestTotal)
			{
				idFurthest = it->first;
				count++;
				FurthestTotal = total;
				
			}

		}

	}
	if (count != 0) 
	{
		if (attackState) 
		{
			getWaypoint(idColest)->setWaypointValue(getWaypoint(idColest)->getWaypointValue() + 1);
		}
		else 
		{
			getWaypoint(idFurthest)->setWaypointValue(getWaypoint(idFurthest)->getWaypointValue() + 1);
		}
	}
}

UnitID UnitManager::getHightestWaypoint()
{
	int value = 0;
	UnitID id = 0;

	for (auto it = mWayPoints.begin(); it != mWayPoints.end(); ++it)
	{

		if (it->second->getWaypointValue() > value) 
		{
			value = it->second->getWaypointValue();
			id = it->first;
		}
	}


	return id;
}

void UnitManager::cleanUp() 
{
	for (auto it = mWayPoints.begin(); it != mWayPoints.end(); ++it)
	{
		it->second->cleanUpStateMechine();
	}

	for (auto it = mUnitMap.begin(); it != mUnitMap.end(); ++it)
	{
		it->second->cleanUpStateMechine();
	}
}


void UnitManager::spawnUnits()
{
	mEnemySpawnCount++;
	//mPowerUpSpawnCount++;

	if (mEnemySpawnCount > gpGame->getDataKeeper()->getDataFile().enemySpawnRate) 
	{
		mEnemySpawnCount = 0;
		createRandomUnit(*gpGame->getSpriteManager()->getSprite(AI_ICON_SPRITE_ID));
	
	}

	/*if (mPowerUpSpawnCount > gpGame->getDataKeeper()->getDataFile().powerSpawnRate)
	{
		mPowerUpSpawnCount = 0;
		createRandomPowerUp(*gpGame->getSpriteManager()->getSprite(POWER_UP_SPRITE_ID));

	}*/

}