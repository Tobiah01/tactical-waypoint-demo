#pragma once
#include "GameMessage.h"
#include "Vector2D.h"
#include "Trackable.h"

class Vector2D;

class DespawnUnitMessage :public GameMessage
{
public:
	DespawnUnitMessage();
	~DespawnUnitMessage();

	void process();

private:

};