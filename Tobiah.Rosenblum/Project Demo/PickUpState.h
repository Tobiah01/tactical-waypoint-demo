#pragma once

#include "StateMachine.h"
#include "PickUpState.h"
#include "FaceArriveSteering.h"
#include "Sprite.h"

class PickUPState : public StateMachineState
{
public:
	PickUPState(const SM_idType& id, UnitID unitID, Sprite* arrow) :StateMachineState(id), mUnit(unitID), mStateSprite(arrow){};

	virtual void onEntrance(); // create Steering
	virtual void onExit(); // delete Steering
	virtual StateTransition* update();

private:
	UnitID mUnit;
	Sprite* mStateSprite;
	FaceArriveSteering* mSteering;
};