#pragma once

#include <Trackable.h>
#include <DeanLibDefines.h>
#include <limits>
#include <Vector2D.h>

#include "Component.h"
#include "PositionComponent.h"
#include "Sprite.h"
#include "Steering.h"
#include "StateMachine.h"
#include <vector>
//#include "CircularQueue.h"
//#include "Transaction.h"
//#include "TransactionProcessor.h"

class PhysicsComponent;
class SteeringComponent;
class Sprite;
class UnitManager;

const Uint32 DEFAULT_QUEUE_CAPACITY = 8;

enum powerType
{
	INVALID = -1,
	HEALTH = 0,
	SPEED = 1,
	DAMAGE = 2
};

struct unitGameStats
{

	float health;
	float damage;
	float damageRadius;
	float collistionRadius;
	float speedPercent;
	float sitghtRange;


};

struct Area
{
	float upperLeftX;// = 0;
	float upperLeftY;// = 0;
	float lowerRightX;// = 0;
	float lowerRightY;// = 0;

};


//class Unit : public TransactionProcessor
class Unit : public Trackable
{
public:
	void draw() const;
	float getFacing() const;
	void update(float elapsedTime);

	PositionComponent* getPositionComponent() const;
	PhysicsComponent* getPhysicsComponent() const;
	SteeringComponent* getSteeringComponent() const;
	float getMaxAcc() const { return mMaxAcc * mStats.speedPercent; };
	float getMaxSpeed() const { return mMaxSpeed * mStats.speedPercent; };
	float getMaxRotAcc() const { return mMaxRotAcc * mStats.speedPercent; };
	float getMaxRotVel() const { return mMaxRotVel * mStats.speedPercent; };
	

	/*inline float getHealth() { return mHealth; }
	inline float getDamage() { return mDamage; }
	inline float getDamageRadius() { return mDamageRadius; }
	inline float getColloston() { return mCollistionRadius; }
	inline float getSpeedPercent() { return mSpeedPercent; }*/
	inline powerType getPowerUpType() { return mPowerUpType; };

	//inline void setHealth(float health) { mHealth = health; }
	//inline void setDamage(float damage) { mDamage = damage; }
	//inline void setDamageRadius(float radius) { mDamageRadius = radius; }
	//inline void setCollistionRadius(float radius) { mCollistionRadius = radius; }
	inline void setPowerUpType(powerType type) { mPowerUpType = type; };
	/*inline void setSpeedPercent(float speed) { mSpeedPercent = speed; }*/
	inline unitGameStats getStats() { return mStats; }
	
	void setShowTarget(bool val) { mShowTarget = val; };

	void setSteering(Steering::SteeringType type, Vector2D targetLoc = ZERO_VECTOR2D, UnitID targetUnitID = INVALID_UNIT_ID);


	void unitPowerUp(powerType powerUp);

	void setNewSprite(Sprite* sprite);

	unitGameStats mStats;
	Area mArea;

	inline void setUpperLeftX(float health) { mArea.upperLeftX = health; }
	inline void setUpperLeftY(float damage) { mArea.upperLeftY = damage; }
	inline void setLowerRightX(float radius) { mArea.lowerRightX = radius; }
	inline void setLowerRightY(float radius) { mArea.lowerRightY = radius; }
	inline void setWaypoint(bool way) { mIsWaypoint = way; };

	
	

	inline void setWaypointValue(int value) { mWaypointValue = value; };
	inline int getWaypointValue() { return mWaypointValue; };
	
	void createStateMechine();

	void cleanUpStateMechine();
	

	

private:
	UnitID mID;
	ComponentID mPhysicsComponentID;
	ComponentID mPositionComponentID;
	ComponentID mSteeringComponentID;
	PositionComponent* mpPositionComponent = NULL;
	Sprite mSprite;
	float mMaxAcc;
	float mMaxSpeed;
	float mMaxRotAcc;
	float mMaxRotVel;
	
	StateMachine* mpStateMachine;
	std::vector<StateMachineState*> mMechineStates;
	std::vector<StateTransition*> mTrasitons;

	bool mShowTarget;
	powerType mPowerUpType;

	bool mIsWaypoint = false;
	int mWaypointValue = 0;
	float mValue = 0;
	int mIntValue = 0;

	


	Unit(const Sprite& sprite);
	virtual ~Unit();

	Unit(Unit&);//invalidate copy constructor
	void operator=(Unit&);//invalidate assignment operator

	friend class UnitManager;
};
