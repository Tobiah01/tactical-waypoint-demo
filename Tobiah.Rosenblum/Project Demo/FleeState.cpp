#include "FleeState.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "Steering.h"
#include "GraphicsSystem.h"
#include <iostream>


void FleeState::onEntrance()
{

	int posX = rand() % gpGame->getGraphicsSystem()->getWidth();
	int posY = rand() % gpGame->getGraphicsSystem()->getHeight();

	gpGame->getUnitManager()->getUnit(mUnit)->setSteering(Steering::FLEE, ZERO_VECTOR2D, PLAYER_UNIT_ID);


	gpGame->getUnitManager()->getUnit(mUnit)->setNewSprite(mStateSprite);
}


void FleeState::onExit()
{



}

StateTransition* FleeState::update()
{

	// if pickup is near
	//if (gpGame->getUnitManager()->isPickupNear(mUnit))
	//{
	//	std::map<TransitionType, StateTransition*>::iterator iter = mTransitions.find(PICKUP_STATE);
	//	if (iter != mTransitions.end())//found?
	//	{
	//		StateTransition* pTransition = iter->second;
	//		return pTransition;
	//	}
	//}


	return NULL;
}