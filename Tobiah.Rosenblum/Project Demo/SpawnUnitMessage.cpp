#include "Game.h"
#include "GameMessageManager.h"
#include "SpawnUnitMessage.h"
#include "UnitManager.h"
#include "SpriteManager.h"

SpawnUnitMessage::SpawnUnitMessage()
	:GameMessage(SPAWM_UNIT_MESSAGE)
{
}

SpawnUnitMessage::~SpawnUnitMessage()
{
}

void SpawnUnitMessage::process()
{
	Sprite* enemySprite = gpGame->getSpriteManager()->getSprite(AI_ICON_SPRITE_ID);

	gpGame->getUnitManager()->createRandomUnit(*enemySprite);
}

