
#include "InputManger.h"
#include "EndLoopMessage.h"
#include "Game.h"

EndLoopMessage::EndLoopMessage()
	:GameMessage(SPAWM_UNIT_MESSAGE)
{
}

EndLoopMessage::~EndLoopMessage()
{
}

void EndLoopMessage::process()
{
	
	gpGame->getInputManger()->setEndLoop(true);
	
}

