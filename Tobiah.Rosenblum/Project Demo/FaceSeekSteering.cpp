
#include "Steering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "SeekSteering.h"
#include "AlignSteering.h"
#include "FaceSeekSteering.h"




FaceSeekSteering::FaceSeekSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID, bool shouldFlee)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);


}


Steering* FaceSeekSteering::getSteering()
{
	SeekSteering* pSeekSteering;
	AlignSteering* pAlignSteering;

	pSeekSteering = new SeekSteering(mOwnerID, mTargetLoc, mTargetID, false);
	pAlignSteering = new AlignSteering(mOwnerID, mTargetLoc, mTargetID, false);

	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);

	PhysicsData data = pOwner->getPhysicsComponent()->getData();

	pSeekSteering->getSteering();
	pAlignSteering->getSteering();

	data.rotAcc = pAlignSteering->getData().rotAcc;

	data.acc = pSeekSteering->getData().acc;

	delete pSeekSteering;
	delete pAlignSteering;

	this->mData = data;
	return this;
}