#include <cassert>

#include "Steering.h"
#include "GroupAlignSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "AlignSteering.h"
#include "DataKeeper.h"

GroupAlignSteering::GroupAlignSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
}

Steering* GroupAlignSteering::getSteering()
{
	float threshold = 0; //gpGame->getDataKeeper()->getDataFile().groupAlingThreshold;
	float strenght = 0;
	float timeToTarget = 0.1;

	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);

	float accMax = pOwner->getPhysicsComponent()->getData().maxAccMagnitude;
	
	Vector2D avrageDir = pOwner->getPhysicsComponent()->getVelocity();

	Vector2D diff;
	Vector2D velDist;

	//are we seeking a location or a unit?
	PhysicsData data = pOwner->getPhysicsComponent()->getData();

	// def Steering
	int count = 1;

	Uint32 cnt = 0;
	for (auto it = gpGame->getUnitManager()->getUnitMap()->begin(); it != gpGame->getUnitManager()->getUnitMap()->end(); ++it, cnt++)
	{

		if (it->first != mOwnerID)
		{

			diff = pOwner->getPositionComponent()->getPosition() - it->second->getPositionComponent()->getPosition();

			if(diff.getLengthSquared() < threshold * threshold)
			{

				avrageDir += it->second->getPhysicsComponent()->getVelocity();

				count++;
			}
		}
	}

	if (count != 0) 
	{
		avrageDir /= count;
	}

	// vel match
	velDist = avrageDir - data.vel;
	velDist /= timeToTarget;

	if (velDist.getLength() > accMax)
	{
		velDist.normalize();
		velDist *= accMax;
	}


	data.acc = avrageDir;





	this->mData = data;
	
	return this;
}

