#include <cassert>
#include <cstdlib>
#include <ctime>
#include <iostream>

#include "Steering.h"
#include "WanderSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "FaceArriveSteering.h"
#include "GraphicsSystem.h"






WanderSteering::WanderSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
}

Steering* WanderSteering::getSteering()
{
	int slowRad = 200;
	
	FaceArriveSteering* arriveSteering;
	float targetSpeed = 0.0;
	Vector2D targetVelocity;

	


	Vector2D diff;
	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);
	PhysicsData data = pOwner->getPhysicsComponent()->getData();
	//are we seeking a location or a unit?

	if (mTargetID != INVALID_UNIT_ID)
	{
		//seeking unit
		Unit* pTarget = gpGame->getUnitManager()->getUnit(mTargetID);
		assert(pTarget != NULL);
		mTargetLoc = pTarget->getPositionComponent()->getPosition();
	}


	diff = mTargetLoc - pOwner->getPositionComponent()->getPosition();
	
	if (diff.getLength() < slowRad)
	{
		float screenWidth = gpGame->getGraphicsSystem()->getWidth();
		float screenHight = gpGame->getGraphicsSystem()->getHeight();


		mTargetLoc.setX(abs(genRandomBinomial() * screenWidth));
		mTargetLoc.setY(abs(genRandomBinomial() * screenHight));
		
	}

	arriveSteering = new FaceArriveSteering(mOwnerID, mTargetLoc, mTargetID, false);
	
	arriveSteering->getSteering();

	data = arriveSteering->getData();
	


	delete arriveSteering;

	this->mData = data;
	return this;
}