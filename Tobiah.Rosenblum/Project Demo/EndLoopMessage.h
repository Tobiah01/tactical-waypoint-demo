#pragma once
#include "GameMessage.h"
#include "Vector2D.h"
#include "Trackable.h"

class Vector2D;

class EndLoopMessage :public GameMessage
{
public:
	EndLoopMessage();
	~EndLoopMessage();

	void process();

private:

};