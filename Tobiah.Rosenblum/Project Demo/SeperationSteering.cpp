#include <cassert>

#include "Steering.h"
#include "SeperationSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "DataKeeper.h"


SeperationSteering::SeperationSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
}

Steering* SeperationSteering::getSteering()
{
	float threshold = 0; //gpGame->getDataKeeper()->getDataFile().seperationThreshold;
	float decayCoefficent = 0; //gpGame->getDataKeeper()->getDataFile().spereationDelayCoeficent;
	float strenght = 0;

	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);

	float accMax = pOwner->getPhysicsComponent()->getData().maxAccMagnitude;


	Vector2D diff;
	
	//are we seeking a location or a unit?
	PhysicsData data = pOwner->getPhysicsComponent()->getData();

	// def Steering

	Uint32 cnt = 0;
	for (auto it = gpGame->getUnitManager()->getUnitMap()->begin(); it != gpGame->getUnitManager()->getUnitMap()->end(); ++it, cnt++)
	{

		if (it->first != mOwnerID)
		{

			diff = pOwner->getPositionComponent()->getPosition() - it->second->getPositionComponent()->getPosition();


			if (diff.getLengthSquared() < threshold * threshold)
			{

				strenght = min(decayCoefficent / (diff.getLength() * diff.getLength()), accMax);



				diff.normalize();
				data.acc += diff*strenght;
			}
		}
	}


	this->mData = data;
	return this;
}

