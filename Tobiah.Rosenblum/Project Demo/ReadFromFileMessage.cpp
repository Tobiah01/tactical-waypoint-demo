#include "Game.h"
#include "GameMessageManager.h"
#include "ReadFromFileMessage.h"
#include "FileReaderWriter.h"



ReadFromFileMessage::ReadFromFileMessage()
	:GameMessage(READ_FROM_FILE_MESSAGE)
{
}

ReadFromFileMessage::~ReadFromFileMessage()
{
}

void ReadFromFileMessage::process()
{
	gpGame->getFileReaderWriter()->readFromFile();
}

