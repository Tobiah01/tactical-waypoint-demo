
#include "Steering.h"
#include "FaceArriveSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "ArriveStreering.h"
#include "AlignSteering.h"





FaceArriveSteering::FaceArriveSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID, bool shouldFlee)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
	

}


Steering* FaceArriveSteering::getSteering()
{

	ArriveStreering* pArriveSteering;
	AlignSteering* pAlignSteering;

	

	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);

	PhysicsData data = pOwner->getPhysicsComponent()->getData();
	
	pAlignSteering = new AlignSteering(mOwnerID, mTargetLoc, mTargetID, false);

	pAlignSteering->getSteering();

	data.rotAcc = pAlignSteering->getData().rotAcc;
	
	pArriveSteering = new ArriveStreering(mOwnerID, mTargetLoc, mTargetID, false);

	pArriveSteering->getSteering();

	data.acc = pArriveSteering->getData().acc;

	delete pArriveSteering;
	delete pAlignSteering;

	this->mData = data;
	return this;
}