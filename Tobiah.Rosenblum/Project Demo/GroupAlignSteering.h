#pragma once
#include <Trackable.h>
#include "Steering.h"

class GroupAlignSteering : public Steering
{

public:
	GroupAlignSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID = INVALID_UNIT_ID);

	virtual Steering* getSteering();

protected:
	
};