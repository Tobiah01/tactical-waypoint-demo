#include <stdio.h>
#include <assert.h>

#include <sstream>
#include <SDL.h>

#include "InputManger.h"
#include "Game.h"
#include "Vector2D.h"
#include "SpawnUnitMessage.h"
#include "DespawnUnitMessage.h"
#include "ReadFromFileMessage.h"
#include "FileReaderWriter.h"
#include "EndLoopMessage.h"
#include "DataKeeper.h"

/*Author: Tobiah Rosenblum Class: 410-02 <Section x> 
Assignment: State Mechiense
Certification of Authenticity: I certify that this assignment is entirely my own work.

*/

InputManger::InputManger()
{
	mMangerEndLoop = false;
}

void InputManger::cleanUp()
{


}

void InputManger::procosesInputs()
{

	SDL_PumpEvents();

	int x, y;
	SDL_GetMouseState(&x, &y);


	const Uint8 *state = SDL_GetKeyboardState(NULL);

	if (SDL_GetMouseState(&x, &y) & SDL_BUTTON(SDL_BUTTON_LEFT))
	{
		if (gpGame->getPlayerIsAlive()) 
		{
			Vector2D pos(x, y);
			GameMessage* pMessage = new PlayerMoveToMessage(pos);
			MESSAGE_MANAGER->addMessage(pMessage, 0);
		}
	}

	//if escape key was down then exit the loop
	if (state[SDL_SCANCODE_ESCAPE])
	{
		GameMessage* pMessage = new EndLoopMessage();
		MESSAGE_MANAGER->addMessage(pMessage, 0);
	}

	// spawn unit
	//if (state[SDL_SCANCODE_A])
	//{
	//	for (int i = 0; i < 1; i++)
	//	{
	//		GameMessage* pMessage = new SpawnUnitMessage();
	//		MESSAGE_MANAGER->addMessage(pMessage, 0);
	//	}
	//}

	//// despawn unit
	//if (state[SDL_SCANCODE_D])
	//{
	//	GameMessage* pMessage = new DespawnUnitMessage();
	//	MESSAGE_MANAGER->addMessage(pMessage, 0);
	//}

	if (state[SDL_SCANCODE_R])
	{
		GameMessage* pMessage = new ReadFromFileMessage();
		MESSAGE_MANAGER->addMessage(pMessage, 0);
	}

	if (state[SDL_SCANCODE_N])
	{
		gpGame->setTextActive(true);
	}

	if (state[SDL_SCANCODE_M])
	{
		gpGame->setTextActive(false);
	}


	if (state[SDL_SCANCODE_Z])
	{
		gpGame->setBoxActive(true);
	}

	if (state[SDL_SCANCODE_X])
	{
		gpGame->setBoxActive(false);
	}

}



InputManger::~InputManger() 
{

	cleanUp();

}


bool InputManger::getEndLoop()
{

	return mMangerEndLoop;
}

void InputManger::setEndLoop(bool loop) 
{
	
	mMangerEndLoop = loop;
}