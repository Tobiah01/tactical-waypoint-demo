#pragma once

#include <Trackable.h>
#include <DeanLibDefines.h>
#include <MemoryPool.h>
#include <map>
#include "PositionComponent.h"
#include "PhysicsComponent.h"
#include "Unit.h"
#include <vector>
#include "GraphicsBuffer.h"

class Unit;
class Sprite;
struct PositionData;
struct PhysicsData;

const UnitID PLAYER_UNIT_ID = 0;


class UnitManager : public Trackable
{
public:
	UnitManager(Uint32 maxSize);
	~UnitManager() { cleanUp(); };

	Unit* createUnit(const Sprite& sprite, bool isPowerUp = false, bool shouldWrap = true, const PositionData& posData = ZERO_POSITION_DATA, const PhysicsData& physicsData = ZERO_PHYSICS_DATA, const UnitID& id = INVALID_UNIT_ID);
	Unit* createPlayerUnit(const Sprite& sprite, bool shouldWrap = true, const PositionData& posData = ZERO_POSITION_DATA, const PhysicsData& physicsData = ZERO_PHYSICS_DATA);
	Unit* createRandomUnit(const Sprite& sprite);
	Unit* createRandomPowerUp(const Sprite& sprite);
	Unit* createWaypoint(const Sprite& sprite, Vector2D pos);
	

	Unit* getUnit(const UnitID& id) const;
	Unit* getWaypoint(const UnitID& id) const;
	void deleteUnit(const UnitID& id);
	void deletePowerUp(const UnitID& id);
	void deleteRandomUnit();

	void drawAll() const;
	void updateAll(float elapsedTime);

	Unit* getPlayerUnit() const { return getUnit(PLAYER_UNIT_ID); };

	std::map<UnitID, Unit*>* getUnitMap() { return &mUnitMap; };

	void checkHealth(UnitID id);
	void checkEnemyVSPlayerRadius(UnitID id);
	void checkPlayerVSEnemtRadius(UnitID id);
	void pickUpPower(UnitID id);
	int calcWaypointValue(UnitID id);
	void getClosestWaypoint();
	UnitID getHightestWaypoint();
	
	void setCurrentBuffer(GraphicsBuffer* buffer) { mBuff = buffer; };
	GraphicsBuffer*  getCurrentBuffer() { return mBuff; };

	void setAttackState(bool attack) { attackState = attack; };
	bool getAttackState() { return attackState; };

	void deleteEnmeies();


	bool isPickupNear(UnitID unitID);
	bool isNearPlayer(UnitID unitID);
	Vector2D getNearbyPickupLoc(UnitID unitID);

	void cleanUp();

	void spawnUnits();

private:
	static UnitID msNextUnitID;
	MemoryPool mPool;
	std::map<UnitID, Unit*> mUnitMap;
	std::map<UnitID, Unit*> mPowerUpMap;
	std::map<UnitID, Unit*> mWayPoints;
	std::vector<int> removeUnits;
	std::vector<int> removePowerUps;
	int mEnemySpawnCount = 0;
	int mPowerUpSpawnCount = 0;
	GraphicsBuffer* mBuff;
	bool attackState;

};

