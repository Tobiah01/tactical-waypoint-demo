#include <stdio.h>
#include <assert.h>

#include <sstream>
#include <SDL.h>

#include "Game.h"
#include "GraphicsSystem.h"
#include "GraphicsBuffer.h"
#include "Font.h"
#include "GraphicsBufferManager.h"
#include "GameMessageManager.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "Timer.h"
#include "PlayerMoveToMessage.h"
#include "ComponentManager.h"
#include "UnitManager.h"
#include "InputManger.h"
#include "DataKeeper.h"
#include "FileReaderWriter.h"

Game* gpGame = NULL;

const int WIDTH = 1024;
const int HEIGHT = 768;
const Uint32 MAX_UNITS = 100;

Game::Game()
	:mpGraphicsSystem(NULL)
	, mpGraphicsBufferManager(NULL)
	, mpSpriteManager(NULL)
	, mpLoopTimer(NULL)
	, mpMasterTimer(NULL)
	, mpFont(NULL)
	, mShouldExit(false)
	, mBackgroundBufferID("")
	, mpMessageManager(NULL)
	, mpComponentManager(NULL)
	, mpInputManger(NULL)
	, mpDataKeeper(NULL)
	, mpFileReaderWriter(NULL)
	,mpUnitManager(NULL)
{
}

Game::~Game()
{
	cleanup();
}

bool Game::init()
{
	mShouldExit = false;

	//create Timers
	mpLoopTimer = new Timer;
	mpMasterTimer = new Timer;

	//create and init GraphicsSystem
	mpGraphicsSystem = new GraphicsSystem();
	bool goodGraphics = mpGraphicsSystem->init( WIDTH, HEIGHT );
	if(!goodGraphics) 
	{
		fprintf(stderr, "failed to initialize GraphicsSystem object!\n");
		return false;
	}

	mpGraphicsBufferManager = new GraphicsBufferManager(mpGraphicsSystem);
	mpSpriteManager = new SpriteManager();


	mpInputManger = new InputManger();
	mpDataKeeper = new DataKeeper();
	mpFileReaderWriter = new FileReaderWriter("DataFile.txt");

	mpMessageManager = new GameMessageManager();
	mpComponentManager = new ComponentManager(MAX_UNITS);
	mpUnitManager = new UnitManager(MAX_UNITS);

	//load buffers
	mpGraphicsBufferManager->loadBuffer(mBackgroundBufferID,"wallpaper.bmp");
	mpGraphicsBufferManager->loadBuffer(mPlayerIconBufferID,"arrow.png");
	mpGraphicsBufferManager->loadBuffer(mEnemyIconBufferID,"enemy-arrow.png");
	mpGraphicsBufferManager->loadBuffer(mEnemyWanderIconBufferID, "Enemy_Arrow_Wander.png");
	mpGraphicsBufferManager->loadBuffer(mEnemyPickupIconBufferID, "Enemy_Arrow_Pickup.png");
	mpGraphicsBufferManager->loadBuffer(mEnemyFleeIconBufferID, "Enemy_Arrow_Flee.png");
	mpGraphicsBufferManager->loadBuffer(mTargetBufferID,"target.png");
	mpGraphicsBufferManager->loadBuffer(mPowerUpBufferID, "PowerUp.png");

	//PowerUp
	//load Font
	mpFont = new Font("cour.ttf", 24);
	
	

	//setup sprites
	GraphicsBuffer* pBackGroundBuffer = mpGraphicsBufferManager->getBuffer( mBackgroundBufferID );
	if( pBackGroundBuffer != NULL )
	{
		mpSpriteManager->createAndManageSprite( BACKGROUND_SPRITE_ID, pBackGroundBuffer, 0, 0, (float)pBackGroundBuffer->getWidth(), (float)pBackGroundBuffer->getHeight() );
	}
	GraphicsBuffer* pPlayerBuffer = mpGraphicsBufferManager->getBuffer( mPlayerIconBufferID );
	Sprite* pArrowSprite = NULL;
	if( pPlayerBuffer != NULL )
	{
		pArrowSprite = mpSpriteManager->createAndManageSprite( PLAYER_ICON_SPRITE_ID, pPlayerBuffer, 0, 0, (float)pPlayerBuffer->getWidth(), (float)pPlayerBuffer->getHeight() );
	}
	GraphicsBuffer* pAIBuffer = mpGraphicsBufferManager->getBuffer(mEnemyIconBufferID);
	Sprite* pEnemyArrow = NULL;
	if (pAIBuffer != NULL)
	{
		pEnemyArrow = mpSpriteManager->createAndManageSprite(AI_ICON_SPRITE_ID, pAIBuffer, 0, 0, (float)pAIBuffer->getWidth(), (float)pAIBuffer->getHeight());
	}

	GraphicsBuffer* pAIWanderBuffer = mpGraphicsBufferManager->getBuffer(mEnemyWanderIconBufferID);
	Sprite* pEnemyWanderArrow = NULL;
	if (pAIWanderBuffer != NULL)
	{
		pEnemyWanderArrow = mpSpriteManager->createAndManageSprite(AI_ICON_WANDER_SPRITE_ID, pAIWanderBuffer, 0, 0, (float)pAIWanderBuffer->getWidth(), (float)pAIWanderBuffer->getHeight());
	}

	GraphicsBuffer* pAIPickupBuffer = mpGraphicsBufferManager->getBuffer(mEnemyPickupIconBufferID);
	Sprite* pEnemyPickupArrow = NULL;
	if (pAIPickupBuffer != NULL)
	{
		pEnemyPickupArrow = mpSpriteManager->createAndManageSprite(AI_ICON_PICKUP_SPRITE_ID, pAIPickupBuffer, 0, 0, (float)pAIPickupBuffer->getWidth(), (float)pAIPickupBuffer->getHeight());
	}

	GraphicsBuffer* pAIFleeBuffer = mpGraphicsBufferManager->getBuffer(mEnemyFleeIconBufferID);
	Sprite* pEnemyFleeArrow = NULL;
	if (pAIFleeBuffer != NULL)
	{
		pEnemyFleeArrow = mpSpriteManager->createAndManageSprite(AI_ICON_FLEE_SPRITE_ID, pAIFleeBuffer, 0, 0, (float)pAIFleeBuffer->getWidth(), (float)pAIFleeBuffer->getHeight());
	}

	GraphicsBuffer* pTargetBuffer = mpGraphicsBufferManager->getBuffer(mTargetBufferID);
	if (pTargetBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(TARGET_SPRITE_ID, pTargetBuffer, 0, 0, (float)pTargetBuffer->getWidth(), (float)pTargetBuffer->getHeight());
	}

	GraphicsBuffer* pPowerUpBuffer = mpGraphicsBufferManager->getBuffer(mPowerUpBufferID);
	Sprite* pPowerUpSprite = NULL;
	if (pPowerUpBuffer != NULL)
	{
		pPowerUpSprite = mpSpriteManager->createAndManageSprite(POWER_UP_SPRITE_ID, pPowerUpBuffer, 0, 0, (float)pPowerUpBuffer->getWidth(), (float)pPowerUpBuffer->getHeight());
	}

	//Data Varbile set up
	mpFileReaderWriter->readFromFile();

	//setup units
	Unit* pUnit = mpUnitManager->createPlayerUnit(*pArrowSprite);
	pUnit->setShowTarget(true);
	pUnit->setSteering(Steering::FACE_ARRIVE, ZERO_VECTOR2D);
	pUnit->mStats.health = mpDataKeeper->getDataFile().playerHealth;
	pUnit->mStats.damage = mpDataKeeper->getDataFile().playerDamage;
	pUnit->mStats.damageRadius = mpDataKeeper->getDataFile().playerDamageRaduis;
	pUnit->mStats.speedPercent = mpDataKeeper->getDataFile().playerSpeedPercent;

	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(100, 100));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(100, 300));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(100, 500));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(100, 700));

	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(300, 100));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(300, 300));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(300, 500));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(300, 700));

	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(500, 100));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(500, 300));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(500, 500));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(500, 700));

	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(700, 100));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(700, 300));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(700, 500));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(700, 700));
	
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(900, 100));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(900, 300));
	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(900, 500));
	//pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(900, 700));

//	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(100, 600));
//	pUnit = mpUnitManager->createWaypoint(*pPowerUpSprite, Vector2D(100, 700));
    
	//mpUnitManager->createUnit(*pPowerUpSprite, true);

	mpUnitManager->setAttackState(false);

	return true;
}

void Game::cleanup()
{
	//delete the timers
	delete mpLoopTimer;
	mpLoopTimer = NULL;
	delete mpMasterTimer;
	mpMasterTimer = NULL;

	delete mpFont;
	mpFont = NULL;

	//delete the graphics system
	delete mpGraphicsSystem;
	mpGraphicsSystem = NULL;

	delete mpGraphicsBufferManager;
	mpGraphicsBufferManager = NULL;
	delete mpSpriteManager;
	mpSpriteManager = NULL;
	delete mpInputManger;
	mpInputManger = NULL;
	delete mpFileReaderWriter;
	mpFileReaderWriter = NULL;
	delete mpDataKeeper;
	mpDataKeeper = NULL;
	delete mpMessageManager;
	mpMessageManager = NULL;
	delete mpUnitManager;
	mpUnitManager = NULL;
	delete mpComponentManager;
	mpComponentManager = NULL;
}

void Game::beginLoop()
{
	mpLoopTimer->start();
}

const float TARGET_ELAPSED_MS = LOOP_TARGET_TIME / 1000.0f;
	
void Game::processLoop()
{
	std::string health = "Health: ";

	if (mPlayerIsAlive) 
	{
		mpUnitManager->updateAll(TARGET_ELAPSED_MS);
		
		health += std::to_string(mpUnitManager->getPlayerUnit()->mStats.health);
	}
	mpComponentManager->update(TARGET_ELAPSED_MS);
	
	//draw background
	Sprite* pBackgroundSprite = mpSpriteManager->getSprite( BACKGROUND_SPRITE_ID );
	GraphicsBuffer* pDest = mpGraphicsSystem->getBackBuffer();
	mpGraphicsSystem->draw(*pDest, *pBackgroundSprite, 0.0f, 0.0f);

	std::string score = "Score: ";
	score += std::to_string(mPlayerScore);

	

	mpGraphicsSystem->writeText(*mpFont, 400, 1, score , BLACK_COLOR);
	mpGraphicsSystem->writeText(*mpFont, 100, 1, health, BLACK_COLOR);
	if (!mPlayerIsAlive)
	{
		mpGraphicsSystem->writeText(*mpFont, 370, 40, "Prese escape to continue", BLACK_COLOR);

	}

	mpUnitManager->setCurrentBuffer(pDest);
	//draw units
	mpUnitManager->drawAll();

	mpGraphicsSystem->fillRegion(*pDest, Vector2D(900, 700), Vector2D(mpGraphicsSystem->getHeight() + 300, mpGraphicsSystem->getWidth() + 300), RED_COLOR);


	//test of fill region
	//mpGraphicsSystem->fillRegion(*pDest, Vector2D(300, 300), Vector2D(500, 500), RED_COLOR);
	mpGraphicsSystem->swap();

	mpMessageManager->processMessagesForThisframe();

	
	mpInputManger->procosesInputs();

	mShouldExit = mpInputManger->getEndLoop();

	if (mPlayerIsAlive)
	{

		mPlayerScore++;
	}
	

}

bool Game::endLoop()
{
	mpLoopTimer->sleepUntilElapsed( LOOP_TARGET_TIME );
	return mShouldExit;
}

float genRandomBinomial()
{
	return genRandomFloat() - genRandomFloat();
}

float genRandomFloat()
{
	float r = (float)rand()/(float)RAND_MAX;
	return r;
}

