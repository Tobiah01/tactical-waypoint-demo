#include <cassert>

#include "Steering.h"
#include "FlockingSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "CohesionSteering.h"
#include "SeperationSteering.h"
#include "GroupAlignSteering.h"
#include "DataKeeper.h"
#include "AlignSteering.h"
//#include <vector>


FlockingSteering::FlockingSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
}

Steering* FlockingSteering::getSteering()
{
	float cohesionLinerWeight = 0;// gpGame->getDataKeeper()->getDataFile().cohisionLineirWieght;
	float cohesionRotWeight = 0;
	
	float seperationLinerWeight = 0; // gpGame->getDataKeeper()->getDataFile().seperationLineirWieght;
	float seperationRotWeight = 0;

	float groupLinerAlignWeight = 0; //gpGame->getDataKeeper()->getDataFile().groupAlignLineirWieght;
	
	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);

	std::vector<startBlendStruct> blending;

	

	//are we seeking a location or a unit?
	
	PhysicsData data = pOwner->getPhysicsComponent()->getData();
	
	
	CohesionSteering* pCohes = new CohesionSteering(mOwnerID, mTargetLoc, mTargetID);
	SeperationSteering* pSep = new SeperationSteering(mOwnerID, mTargetLoc, mTargetID);
	GroupAlignSteering* pGroup = new GroupAlignSteering(mOwnerID, mTargetLoc, mTargetID);

	pCohes->getSteering();
	pSep->getSteering();
	pGroup->getSteering();

	startBlendStruct blendSep;
	blendSep.blendLinerWeight = seperationLinerWeight;
	blendSep.blendSpeed = pSep->getData().acc;

	blending.push_back(blendSep);

	startBlendStruct blendCohes;
	blendCohes.blendLinerWeight = cohesionLinerWeight;
	blendCohes.blendSpeed = pCohes->getData().acc;

	blending.push_back(blendCohes);

	startBlendStruct blendGroup;
	blendGroup.blendLinerWeight = groupLinerAlignWeight;
	blendGroup.blendSpeed = pGroup->getData().acc;

	blending.push_back(blendGroup);


	endBlendStruct blendDone = Blend(blending);


	AlignSteering* pFace = new AlignSteering(mOwnerID, data.vel+pOwner->getPositionComponent()->getPosition());
	pFace->getSteering();

	data.acc = blendDone.finalBlendSpeed;
	data.rotAcc = pFace->getData().rotAcc;

	delete pCohes;
	delete pSep;
	delete pGroup;
	delete pFace;

	this->mData = data;
	return this;
}

