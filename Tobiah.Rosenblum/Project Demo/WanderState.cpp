#include "WanderState.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "Steering.h"
#include "GraphicsSystem.h"
#include <iostream>

void WanderState::onEntrance() 
{
	int posX = rand() % gpGame->getGraphicsSystem()->getWidth();
	int posY = rand() % gpGame->getGraphicsSystem()->getHeight();

	gpGame->getUnitManager()->getUnit(mUnit)->setSteering(Steering::WANDER_STEERING, Vector2D(posX, posY));
	gpGame->getUnitManager()->getUnit(mUnit)->setNewSprite(mStateSprite);
	
}


void WanderState::onExit() 
{


}


StateTransition* WanderState::update() 
{

	// if pickup is near
	//if (gpGame->getUnitManager()->isPickupNear(mUnit)) 
	//{
	//	std::map<TransitionType, StateTransition*>::iterator iter = mTransitions.find(PICKUP_STATE);
	//	if (iter != mTransitions.end())//found?
	//	{
	//		StateTransition* pTransition = iter->second;
	//		return pTransition;
	//	}	
	//}

	//// if needs to flee
	//if (gpGame->getUnitManager()->getUnit(mUnit)->getStats().health <= 1.0f) 
	//{
	//
	//	std::map<TransitionType, StateTransition*>::iterator iter = mTransitions.find(FLEE_STATE);
	//	if (iter != mTransitions.end())//found?
	//	{
	//		StateTransition* pTransition = iter->second;
	//		return pTransition;
	//	}	
	//}

	//// if needs to attack player
	//if (gpGame->getUnitManager()->isNearPlayer(mUnit))
	//{
	//	std::map<TransitionType, StateTransition*>::iterator iter = mTransitions.find(SEEK_STATE);
	//	if (iter != mTransitions.end())//found?
	//	{
	//		StateTransition* pTransition = iter->second;
	//		return pTransition;
	//	}
	//}


	return NULL;
}
