#include "ComponentManager.h"
#include "SteeringComponent.h"
#include "SeekSteering.h"
#include "ArriveStreering.h"
#include "AlignSteering.h"
#include "FaceArriveSteering.h"
#include "FaceSeekSteering.h"
#include "WanderSteering.h"
#include "WanderChaseSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "SeperationSteering.h"
#include "CohesionSteering.h"
#include "FlockingSteering.h"


SteeringComponent::SteeringComponent(const ComponentID& id, const ComponentID& physicsComponentID) 
	:Component(id)
	, mPhysicsComponentID(physicsComponentID)
	, mpSteering(NULL)
{
}

SteeringComponent::~SteeringComponent()
{
	delete mpSteering;
}

void SteeringComponent::applySteering(PhysicsComponent& physicsComponent)
{
	if (mpSteering != NULL)
	{
		//allow Steering to run
		mpSteering->update();
		//set physics data to that of the Steering
		physicsComponent.setData(mpSteering->getData());
		//update component's data
		mData.targetLoc = mpSteering->getTargetLoc();
	}
}

void SteeringComponent::setData(const SteeringData& data)
{
	mData = data;

	switch (data.type)
	{
		case Steering::SEEK:
		{
			//cleanup old steering - todo: check for already existing steering and reuse if possible
			delete mpSteering;
			//create new steering
			mpSteering = new SeekSteering(data.ownerID, data.targetLoc, data.targetID, false);
			break;
		}
		case Steering::FLEE:
		{
			delete mpSteering;

			mpSteering = new SeekSteering(data.ownerID, data.targetLoc, data.targetID, true);
			break;
		}
		case Steering::ARRIVE:
		{
			delete mpSteering;
			
			mpSteering = new ArriveStreering(data.ownerID, data.targetLoc, data.targetID, false);
			break;
		}
		case Steering::ALIGN_FACE:
		{
			delete mpSteering;
			
				mpSteering = new AlignSteering(data.ownerID, data.targetLoc, data.targetID, false);
			break;
		}
		case Steering::FACE_ARRIVE:
		{
			delete mpSteering;

			mpSteering = new FaceArriveSteering(data.ownerID, data.targetLoc, data.targetID, false);
			break;
		}
		case Steering::SEEK_FACE:
		{
			delete mpSteering;

			mpSteering = new FaceSeekSteering(data.ownerID, data.targetLoc, data.targetID, false);
			break;
		}
		case Steering::WANDER_STEERING:
		{
			delete mpSteering;

			mpSteering = new WanderSteering(data.ownerID, data.targetLoc, data.targetID);
			break;
		}
		case Steering::WANDER_CHASE:
		{
			delete mpSteering;

			mpSteering = new WanderChaseSteering(data.ownerID, data.targetLoc, data.targetID);
				
			break;
		}
		case Steering:: SEPERATION:
		{
			delete mpSteering;

			mpSteering = new SeperationSteering(data.ownerID, data.targetLoc, data.targetID);

			break;
		}
		case Steering::COHESION_STEERING:
		{
			delete mpSteering;

			mpSteering = new CohesionSteering(data.ownerID, data.targetLoc, data.targetID);

			break;
		}
		case Steering::FLOKING_STEERING:
		{
			delete mpSteering;

			mpSteering = new FlockingSteering(data.ownerID, data.targetLoc, data.targetID);

			break;
		}
		default:
		{

		}
	};
}

