#include "PickUpState.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "Steering.h"
#include "GraphicsSystem.h"
#include <iostream>


void PickUPState::onEntrance() 
{
	gpGame->getUnitManager()->getUnit(mUnit)->setSteering(Steering::FACE_ARRIVE, gpGame->getUnitManager()->getNearbyPickupLoc(mUnit));
	gpGame->getUnitManager()->getUnit(mUnit)->setNewSprite(mStateSprite);
}


void PickUPState::onExit() 
{



} 

StateTransition* PickUPState::update()
{


	//// if not near pick up
	//if (!gpGame->getUnitManager()->isPickupNear(mUnit))
	//{
	//	std::map<TransitionType, StateTransition*>::iterator iter = mTransitions.find(WANDER_STATE);
	//	if (iter != mTransitions.end()) //found?
	//	{
	//		StateTransition* pTransition = iter->second;
	//		return pTransition;
	//	}
	//}





	return NULL;
}