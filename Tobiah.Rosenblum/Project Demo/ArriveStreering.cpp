#include <cassert>

#include "Steering.h"
#include "ArriveStreering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"






ArriveStreering::ArriveStreering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID, bool shouldFlee)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
}

Steering* ArriveStreering::getSteering() 
{

	int slowRad = 200;
	float timeToTarget = 0.1;
	int targetRad = 0;

	float targetSpeed;
	Vector2D targetVelocity;


	Vector2D diff;
	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);
	PhysicsData data = pOwner->getPhysicsComponent()->getData();
	//are we seeking a location or a unit?

	//if (mTargetID != INVALID_UNIT_ID)
	//{
	//	//seeking unit
	//	Unit* pTarget = gpGame->getUnitManager()->getUnit(mTargetID);
	//	assert(pTarget != NULL);
	//	mTargetLoc = pTarget->getPositionComponent()->getPosition();
	//}


	diff = mTargetLoc - pOwner->getPositionComponent()->getPosition();
	

	if (diff.getLength() < targetRad)
	{
	
		return NULL;
	}

	if (diff.getLength() > slowRad)
	{

		targetSpeed = pOwner->getMaxSpeed();
	}else
	{
		targetSpeed = pOwner->getMaxSpeed() * diff.getLength() / slowRad;

	}

	targetVelocity = diff;
	targetVelocity.normalize();
	targetVelocity *= targetSpeed;
	
	data.acc = targetVelocity - data.vel;

	data.acc /= timeToTarget;

	if (data.acc.getLength() > pOwner->getMaxAcc()) 
	{
		data.acc.normalize();
		data.acc *= pOwner->getMaxAcc();
	}


	//data.acc = diff;
	data.rotVel = 0.0f;
	this->mData = data;
	return this;
}