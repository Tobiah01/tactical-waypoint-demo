#pragma once

#include <Trackable.h>
#include "Steering.h"

class WanderChaseSteering : public Steering
{
public:
	WanderChaseSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID = INVALID_UNIT_ID);

protected:
	virtual Steering* getSteering();


};