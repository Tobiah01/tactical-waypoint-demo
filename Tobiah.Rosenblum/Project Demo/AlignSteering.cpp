#include <cassert>

#define _USE_MATH_DEFINES
#include  <math.h>
#include "Steering.h"
#include "AlignSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"







AlignSteering::AlignSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID, bool shouldFlee)
	: Steering()
{
	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
}

// For this function being the getsteering function I looked at Daniel Konopka's code with his promisson 

Steering* AlignSteering::getSteering()
{
	float targetRotation;
	float rotation;
	float rotationSize;
	float angularAcc;
	Vector2D targetVelocity;

	float slowRot = 0.25 * M_PI;
	float timeToTarge = 0.001;
	float targetRot = M_PI * 0.1;



	Vector2D diff;
	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);
	PhysicsData data = pOwner->getPhysicsComponent()->getData();

	if (mTargetID != INVALID_UNIT_ID)
	{
		//seeking unit
		Unit* pTarget = gpGame->getUnitManager()->getUnit(mTargetID);
		assert(pTarget != NULL);
		mTargetLoc = pTarget->getPositionComponent()->getPosition();
	}

	

	rotation = atan2(mTargetLoc.getY() - pOwner->getPositionComponent()->getPosition().getY(), mTargetLoc.getX() - pOwner->getPositionComponent()->getPosition().getX());
	rotation -= pOwner->getPositionComponent()->getFacing();


	// mapping 
	if (rotation > M_PI)
	{
		rotation -= (2.0 * M_PI);
	}
	else if (rotation < (-1.0 * M_PI))
	{
		rotation += (2.0 * M_PI);
	}

	rotation += M_PI / 2.0;

	rotationSize = abs(rotation);

	

	if (rotationSize < targetRot)
	{
		targetRotation = data.rotVel;
	}

	if (rotationSize > slowRot)
	{
		targetRotation = data.maxRotAcc;
	}
	else
	{
		targetRotation = data.maxRotAcc * (rotationSize / slowRot);
	}

	targetRotation *= rotation / rotationSize;

	data.rotAcc = targetRotation - data.rotVel;
	data.rotAcc /= timeToTarge;

	angularAcc = abs(data.rotAcc);

	if (angularAcc > data.maxRotAcc)
	{
		data.rotAcc /= angularAcc;
		data.rotAcc *= data.maxRotAcc;
	}





	data.vel = 0.0;
	data.acc = 0.0;


	//data.acc = diff;
	this->mData = data;
	return this;
}