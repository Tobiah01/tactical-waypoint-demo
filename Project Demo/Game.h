#pragma once

#include "Trackable.h"
#include "PerformanceTracker.h"
#include "Defines.h"
#include <string>

class GraphicsSystem;
class GraphicsBuffer;
class Font;
class GraphicsBufferManager;
class SpriteManager;
class KinematicUnit;
class GameMessageManager;
class Timer;
class ComponentManager;
class UnitManager;
class InputManger;
class DataKeeper;
class FileReaderWriter;

const IDType BACKGROUND_SPRITE_ID = 0;
const IDType PLAYER_ICON_SPRITE_ID = 1;
const IDType AI_ICON_SPRITE_ID = 2;
const IDType AI_ICON_WANDER_SPRITE_ID = 3;
const IDType AI_ICON_PICKUP_SPRITE_ID = 4;
const IDType AI_ICON_FLEE_SPRITE_ID = 5;
const IDType TARGET_SPRITE_ID = 6;
const IDType POWER_UP_SPRITE_ID = 7;

const float LOOP_TARGET_TIME = 33.3f;//how long should each frame of execution take? 30fps = 33.3ms/frame

class Game:public Trackable
{
public:
	Game();
	~Game();

	bool init();//returns true if no errors, false otherwise
	void cleanup();

	//game loop
	void beginLoop();
	void processLoop();
	
	bool endLoop();

	inline GraphicsSystem* getGraphicsSystem() const { return mpGraphicsSystem; };
	inline GraphicsBufferManager* getGraphicsBufferManager() const { return mpGraphicsBufferManager; };
	inline SpriteManager* getSpriteManager() const { return mpSpriteManager; };
	inline GameMessageManager* getMessageManager() { return mpMessageManager; };
	inline ComponentManager* getComponentManager() { return mpComponentManager; };
	inline InputManger* getInputManger() { return mpInputManger; };
	inline DataKeeper* getDataKeeper() { return mpDataKeeper; };
	inline FileReaderWriter* getFileReaderWriter() { return mpFileReaderWriter; };
	inline UnitManager* getUnitManager() { return mpUnitManager; };
	inline Timer* getMasterTimer() const { return mpMasterTimer; };
	inline Font* getFont() const { return mpFont; };
	inline double getCurrentTime() const { return mpMasterTimer->getElapsedTime(); };
	inline bool getPlayerIsAlive() { return mPlayerIsAlive; };
	inline void setPlayerIsAlive(bool playerIs) { mPlayerIsAlive = playerIs; };
	inline double getPlayerScore() { return mPlayerScore; }
	inline void setPlayerScore(double score) { mPlayerScore = score; };


	inline void setTextActive(bool text) { TextActive = text; };
	inline bool getTextActive(){ return TextActive; };

	inline void setBoxActive(bool box) { BoxActive = box; };
	inline bool getBoxActive(){ return BoxActive; };



private:
	GraphicsSystem* mpGraphicsSystem;
	GraphicsBufferManager* mpGraphicsBufferManager;
	SpriteManager* mpSpriteManager;
	GameMessageManager* mpMessageManager;
	ComponentManager* mpComponentManager;
	UnitManager* mpUnitManager;
	Font* mpFont;
	Timer* mpLoopTimer;
	Timer* mpMasterTimer;
	InputManger* mpInputManger;
	DataKeeper* mpDataKeeper;
	FileReaderWriter* mpFileReaderWriter;
	bool mShouldExit;
	
	//should be somewhere else
	GraphicsBufferID mBackgroundBufferID = "woods";
	GraphicsBufferID mPlayerIconBufferID = "player";
	GraphicsBufferID mEnemyIconBufferID = "enemy";
	GraphicsBufferID mEnemyWanderIconBufferID = "enemy_wander";
	GraphicsBufferID mEnemyPickupIconBufferID = "enemy_pickup";
	GraphicsBufferID mEnemyFleeIconBufferID = "enemy_flee";
	GraphicsBufferID mTargetBufferID = "target";
	GraphicsBufferID mPowerUpBufferID = "powerUp";
	double mPlayerScore = 0.0;
	bool mPlayerIsAlive = true;
	
	bool TextActive = true;
	bool BoxActive = false;

};

float genRandomBinomial();//range -1:1 from "Artificial Intelligence for Games", Millington and Funge
float genRandomFloat();//range 0:1 from "Artificial Intelligence for Games", Millington and Funge

extern Game* gpGame;
extern PerformanceTracker* gpPerformanceTracker;

