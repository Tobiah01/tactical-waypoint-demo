#include "Blending.h"

endBlendStruct Blend(std::vector<startBlendStruct> blends)
{
	endBlendStruct doneBlending;
	doneBlending.finalBlendSpeed = 0;


	for each (startBlendStruct flock in blends)
	{
		doneBlending.finalBlendSpeed += flock.blendSpeed*flock.blendLinerWeight;
	}


	return doneBlending;
}

