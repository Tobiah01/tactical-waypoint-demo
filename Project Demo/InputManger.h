#pragma once

#include "Trackable.h"
#include "PerformanceTracker.h"
#include "Defines.h"
#include "PlayerMoveToMessage.h"
#include "GameMessageManager.h"
#include <string>




class InputManger:public Trackable 
{

public:
	
	InputManger();
	~InputManger();

	void cleanUp();

	void procosesInputs();
	
	bool getEndLoop();
	void setEndLoop(bool loop);

	// vars


private:

	bool mMangerEndLoop;





};