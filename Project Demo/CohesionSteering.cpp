#include <cassert>

#include "Steering.h"
#include "CohesionSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "SeekSteering.h"
#include "CohesionSteering.h"
#include "DataKeeper.h"
#include "ArriveStreering.h"

CohesionSteering::CohesionSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
}

Steering* CohesionSteering::getSteering()
{
	float threshold = 0;// gpGame->getDataKeeper()->getDataFile().cohesionThreshold;
	float strenght = 0;

	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);

	float accMax = pOwner->getPhysicsComponent()->getData().maxAccMagnitude;


	Vector2D diff;

	//are we seeking a location or a unit?
	PhysicsData data = pOwner->getPhysicsComponent()->getData();

	Vector2D avarage = pOwner->getPositionComponent()->getPosition();
	float count = 1;

	Uint32 cnt = 0;
	for (auto it = gpGame->getUnitManager()->getUnitMap()->begin(); it != gpGame->getUnitManager()->getUnitMap()->end(); ++it, cnt++)
	{

		if (it->first != mOwnerID)
		{

			diff = pOwner->getPositionComponent()->getPosition() - it->second->getPositionComponent()->getPosition();

			if (diff.getLengthSquared() < threshold * threshold) {

				avarage += it->second->getPositionComponent()->getPosition();
				count++;
			}

		}
	}

	if (count != 0) 
	{
		avarage /= count;
	
	}

	SeekSteering* pSeek = new SeekSteering(mOwnerID, avarage, INVALID_UNIT_ID, false);

	pSeek->getSteering();

	data.acc = pSeek->getData().acc;

	delete pSeek;

	this->mData = data;
	return this;
}

