#include "SeekState.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "Steering.h"
#include "GraphicsSystem.h"
#include <iostream>


void SeekState::onEntrance()
{

	gpGame->getUnitManager()->getUnit(mUnit)->setSteering(Steering::SEEK, ZERO_VECTOR2D, PLAYER_UNIT_ID);

	gpGame->getUnitManager()->getUnit(mUnit)->setNewSprite(mStateSprite);

}


void SeekState::onExit()
{



}

StateTransition* SeekState::update()
{
//	 if pickup is near
	//if (gpGame->getUnitManager()->isPickupNear(mUnit))
	//{
	//	std::map<TransitionType, StateTransition*>::iterator iter = mTransitions.find(PICKUP_STATE);
	//	if (iter != mTransitions.end())//found?
	//	{
	//		StateTransition* pTransition = iter->second;
	//		return pTransition;
	//	}
	//}

	//// if needs to flee
	//if (gpGame->getUnitManager()->getUnit(mUnit)->getStats().health <= 1.0f)
	//{

	//	std::map<TransitionType, StateTransition*>::iterator iter = mTransitions.find(FLEE_STATE);
	//	if (iter != mTransitions.end())//found?
	//	{
	//		StateTransition* pTransition = iter->second;
	//		return pTransition;
	//	}
	//}

	//// if away from player wander
	//if (!gpGame->getUnitManager()->isNearPlayer(mUnit))
	//{
	//	std::map<TransitionType, StateTransition*>::iterator iter = mTransitions.find(WANDER_STATE);
	//	if (iter != mTransitions.end())//found?
	//	{
	//		StateTransition* pTransition = iter->second;
	//		return pTransition;
	//	}
	//}

	return NULL;
}