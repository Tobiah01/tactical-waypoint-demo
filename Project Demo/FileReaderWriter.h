#pragma once
#include <iostream>
#include <fstream>
#include "Trackable.h"
#include "DataKeeper.h"
#include <string>


class FileReaderWriter 
{

public:



	FileReaderWriter(std::string name);
	~FileReaderWriter();

	void cleanUp();

	void writeToFile();
	void readFromFile();

private:
	std::string mFileName;
	


};





