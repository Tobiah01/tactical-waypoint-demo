#pragma once
#include "GameMessage.h"
#include "Vector2D.h"
#include "Trackable.h"

class Vector2D;

class SpawnUnitMessage :public GameMessage
{
public:
	SpawnUnitMessage();
	~SpawnUnitMessage();

	void process();

private:
	
};