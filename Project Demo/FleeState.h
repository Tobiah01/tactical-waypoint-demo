#pragma once

#include "StateMachine.h"
#include "FleeState.h"
#include "SeekSteering.h"
#include "Sprite.h"

class FleeState : public StateMachineState
{
public:
	FleeState(const SM_idType& id, UnitID unitID, Sprite* arrow) :StateMachineState(id), mUnit(unitID), mStateSprite(arrow) {};

	virtual void onEntrance(); // create Steering
	virtual void onExit(); // delete Steering
	virtual StateTransition* update();
private:
	UnitID mUnit;
	Sprite* mStateSprite;
	SeekSteering* mSteering;
};