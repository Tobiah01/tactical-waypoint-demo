#pragma once

#include "StateMachine.h"
#include "WanderSteering.h"
#include "Sprite.h"


class WanderState : public StateMachineState
{
public:
	WanderState(const SM_idType& id, int unitID, Sprite* arrow) :StateMachineState(id), mUnit(unitID), mStateSprite(arrow) {};

	virtual void onEntrance(); // create Steering
	virtual void onExit(); // delete Steering
	virtual StateTransition* update();

private:
	UnitID mUnit;
	Sprite* mStateSprite;
	WanderSteering* mSteering;
};