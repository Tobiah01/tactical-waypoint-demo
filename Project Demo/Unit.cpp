#include "Unit.h"
#include <assert.h>

#include "Game.h"
#include "GraphicsSystem.h"
#include "Component.h"
#include "PositionComponent.h"
#include "PhysicsComponent.h"
#include "SteeringComponent.h"
#include "ComponentManager.h"
#include "SpriteManager.h"
#include "Sprite.h"
#include "UnitManager.h"
#include "GraphicsSystem.h"
#include "Steering.h"
#include "WanderState.h"
#include "PickUpState.h"
#include "SeekState.h"
#include "FleeState.h"
#include "ToWaypointState.h"
#include "RecalaclState.h"
#include "DataKeeper.h"
#include "GraphicsBuffer.h"
#include "Font.h"

Unit::Unit(const Sprite& sprite) 
	:mSprite(sprite)
	,mPositionComponentID(INVALID_COMPONENT_ID)
	,mPhysicsComponentID(INVALID_COMPONENT_ID)
	,mSteeringComponentID(INVALID_COMPONENT_ID)
	,mShowTarget(false)
{
	
}

Unit::~Unit()
{
	cleanUpStateMechine();

	
}

void Unit::draw() const
{
	PositionComponent* pPosition = getPositionComponent();
	assert(pPosition != NULL);
	const Vector2D& pos = pPosition->getPosition();
	gpGame->getGraphicsSystem()->draw(mSprite, pos.getX(), pos.getY(), pPosition->getFacing());
		
	if (!mIsWaypoint) 
	{

		gpGame->getGraphicsSystem()->outlineRegoin(*gpGame->getUnitManager()->getCurrentBuffer(), Vector2D(mArea.upperLeftX, mArea.upperLeftY),
			Vector2D(mArea.lowerRightX, mArea.lowerRightY), RED_COLOR);
	}

	if (mIsWaypoint) 
	{
		
		if (gpGame->getTextActive()) 
		{
			gpGame->getGraphicsSystem()->writeText(*gpGame->getFont(), pos.getX(), pos.getY(), std::to_string(mWaypointValue), BLACK_COLOR);
		}


		if (gpGame->getBoxActive())
		{
			gpGame->getGraphicsSystem()->outlineRegoin(*gpGame->getUnitManager()->getCurrentBuffer(), Vector2D(mArea.upperLeftX, mArea.upperLeftY),
				Vector2D(mArea.lowerRightX, mArea.lowerRightY), RED_COLOR);

		}
	}


	if (mShowTarget)
	{
		SteeringComponent* pSteering = getSteeringComponent();
		assert(pSteering != NULL);
		const Vector2D& targetLoc = pSteering->getTargetLoc();
		if (&targetLoc != &ZERO_VECTOR2D)
		{
			Sprite* pTargetSprite = gpGame->getSpriteManager()->getSprite(TARGET_SPRITE_ID);
			assert(pTargetSprite != NULL);
			gpGame->getGraphicsSystem()->draw(*pTargetSprite, targetLoc.getX(), targetLoc.getY());
		}
	}
}

void Unit::update(float elapsedTime) 
{
	if (mID != PLAYER_UNIT_ID && !mIsWaypoint) 
	{
		mpStateMachine->update();
	}

	PositionComponent* pPosition = getPositionComponent();
	assert(pPosition != NULL);
	const Vector2D& pos = pPosition->getPosition();
	
	

	//Vector2D(pos.getX() - mStats.damageRadius, pos.getY() - mStats.damageRadius);
	mArea.lowerRightX = (pos.getX() + (mSprite.getWidth() / 2)) - mStats.damageRadius;
	mArea.lowerRightY = (pos.getY() + (mSprite.getHeight() / 2)) - mStats.damageRadius;

	mArea.upperLeftX = (pos.getX() + (mSprite.getWidth() / 2)) + mStats.damageRadius;
	mArea.upperLeftY = (pos.getY() + (mSprite.getHeight() / 2)) + mStats.damageRadius;



}

float Unit::getFacing() const
{
	PositionComponent* pPosition = getPositionComponent();
	assert(pPosition != NULL);
	return pPosition->getFacing();
}

PositionComponent* Unit::getPositionComponent() const
{
	return mpPositionComponent;
}

PhysicsComponent* Unit::getPhysicsComponent() const
{
	PhysicsComponent* pComponent = gpGame->getComponentManager()->getPhysicsComponent(mPhysicsComponentID);
	return pComponent;
}

SteeringComponent* Unit::getSteeringComponent() const
{
	SteeringComponent* pComponent = gpGame->getComponentManager()->getSteeringComponent(mSteeringComponentID);
	return pComponent;
}

void Unit::setSteering(Steering::SteeringType type, Vector2D targetLoc /*= ZERO_VECTOR2D*/, UnitID targetUnitID /*= INVALID_UNIT_ID*/)
{
	SteeringComponent* pSteeringComponent = getSteeringComponent();
	if (pSteeringComponent != NULL)
	{
		pSteeringComponent->setData(SteeringData(type, targetLoc, mID, targetUnitID));
	}
}




void Unit::unitPowerUp(powerType powerUp)
{
	
	switch (powerUp)
	{
	case HEALTH:
		mStats.health += gpGame->getDataKeeper()->getDataFile().powerUpHealth;

		break;

	case SPEED:
		mStats.speedPercent += gpGame->getDataKeeper()->getDataFile().powerUpSpeed;

		break;

	case DAMAGE:
		mStats.damage += gpGame->getDataKeeper()->getDataFile().powerUpDamage;

		break;


	default:
		break;
	}


	

}


void Unit::setNewSprite(Sprite* sprite)
{
	mSprite = *sprite;

}

void Unit::createStateMechine() 
{
	mpStateMachine = new StateMachine();
	// states

	StateMachineState* pToWaypointSate = new ToWaypointState(0, mID, gpGame->getSpriteManager()->getSprite(AI_ICON_FLEE_SPRITE_ID));

	StateMachineState* pRecalcSate = new RecalaclState(1, mID, gpGame->getSpriteManager()->getSprite(AI_ICON_FLEE_SPRITE_ID));


	mMechineStates.push_back(pToWaypointSate);
	mMechineStates.push_back(pRecalcSate);

	// transitons
	StateTransition* pToWaypointTrastion = new StateTransition(TO_WAYPOINT_STATE, 0);
	StateTransition* pRecalcTrastion = new StateTransition(RECAALAC_STATE, 1);

	mTrasitons.push_back(pToWaypointTrastion);
	mTrasitons.push_back(pRecalcTrastion);
	

	// ToWaypoint Tanstions
	pToWaypointSate->addTransition(pRecalcTrastion);
	

	//Recalc Transtions
	pRecalcSate->addTransition(pToWaypointTrastion);

	mpStateMachine->addState(pToWaypointSate);
	mpStateMachine->addState(pRecalcSate);

	mpStateMachine->setInitialStateID(0);
	

}

void Unit::cleanUpStateMechine()
{
	

	for (int i = 0; i < mMechineStates.size(); i++)
	{
		delete mMechineStates[i];
	}

	mMechineStates.clear();



	for (int i = 0; i < mTrasitons.size(); i++)
	{
		delete mTrasitons[i];
	}

	mTrasitons.clear();


	delete mpStateMachine;

}