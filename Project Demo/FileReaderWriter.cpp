#include "FileReaderWriter.h"
#include "DataKeeper.h"
#include "Game.h"
#include <iostream>
#include <fstream>

FileReaderWriter::FileReaderWriter(std::string name)
{
	mFileName = name;


};

FileReaderWriter::~FileReaderWriter()
{

	writeToFile();

};

void FileReaderWriter::readFromFile() 
{
	std::string line;
	std::ifstream file(mFileName);
	int counter = 0;
	if (file.is_open()) 
	{
		// Player:
		std::getline(file, line);
		std::cout << line << '\n';

		// Player Health
		std::getline(file, line);
		std::cout << line << '\n';

		// Health
		std::getline(file, line);
		gpGame->getDataKeeper()->setPlayerHealth(std::stof(line));
		std::cout << line << '\n';
		
		// Player DamageRaduis
		std::getline(file, line);
		std::cout << line << '\n';
		
		// DamageRaduis
		std::getline(file, line);
		gpGame->getDataKeeper()->setPlayerDamageRaduis(std::stof(line));
		std::cout << line << '\n';

		// Player Damge
		std::getline(file, line);
		std::cout << line << '\n';

		// Damge
		std::getline(file, line);
		gpGame->getDataKeeper()->setPlayerDamage(std::stof(line));
		std::cout << line << '\n';
		
		// Player SpeedPercent
		std::getline(file, line);
		std::cout << line << '\n';

		// SpeedPercent
		std::getline(file, line);
		gpGame->getDataKeeper()->setPlayerSpeedPercent(std::stof(line));
		std::cout << line << '\n';

		// Enemies:
		std::getline(file, line);
		std::cout << line << '\n';

		//Enemy Health
		std::getline(file, line);
		std::cout << line << '\n';

		//Health
		std::getline(file, line);
		gpGame->getDataKeeper()->setEnemyHealth(std::stof(line));
		std::cout << line << '\n';

		// Enemy DamageRaduis
		std::getline(file, line);
		std::cout << line << '\n';

		// DamageRaduis
		std::getline(file, line);
		gpGame->getDataKeeper()->setEnemyDamageRaduis(std::stof(line));
		std::cout << line << '\n';

		// Enemy Damage
		std::getline(file, line);
		std::cout << line << '\n';

		// Damage
		std::getline(file, line);
		gpGame->getDataKeeper()->setEnemyDamage(std::stof(line));
		std::cout << line << '\n';

		// Enemy SpeedPecent
		std::getline(file, line);
		std::cout << line << '\n';

		// SpeedPecent
		std::getline(file, line);
		gpGame->getDataKeeper()->setEnemySpeedPercent(std::stof(line));
		std::cout << line << '\n';

		// Enemy SpawnRate
		std::getline(file, line);
		std::cout << line << '\n';

		// SpawnRate
		std::getline(file, line);
		gpGame->getDataKeeper()->setEnemySpawnRate(std::stof(line));
		std::cout << line << '\n';

		// Enemy SightRange
		std::getline(file, line);
		std::cout << line << '\n';

		// SightRange
		std::getline(file, line);
		gpGame->getDataKeeper()->setEnemySightRange(std::stof(line));
		std::cout << line << '\n';

		// PowerUps
		std::getline(file, line);
		std::cout << line << '\n';

		// PowerUp SpawnRate
		std::getline(file, line);
		std::cout << line << '\n';

		// SpawnRate
		std::getline(file, line);
		gpGame->getDataKeeper()->setPowerUpSpawnRate(std::stof(line));
		std::cout << line << '\n';

		// PowerUp CollistionRadius
		std::getline(file, line);
		std::cout << line << '\n';

		// CollistionRadius
		std::getline(file, line);
		gpGame->getDataKeeper()->setPowerUpCollistionRadius(std::stof(line));
		std::cout << line << '\n';

		// PowerUp Speed
		std::getline(file, line);
		std::cout << line << '\n';

		// Speed
		std::getline(file, line);
		gpGame->getDataKeeper()->setPowerUpSpeed(std::stof(line));
		std::cout << line << '\n';

		// PowerUp Damage
		std::getline(file, line);
		std::cout << line << '\n';

		// Damage
		std::getline(file, line);
		gpGame->getDataKeeper()->setPowerUpDamge(std::stof(line));
		std::cout << line << '\n';

		// PowerUp Health
		std::getline(file, line);
		std::cout << line << '\n';

		// Health
		std::getline(file, line);
		gpGame->getDataKeeper()->setPowerUpHealth(std::stof(line));
		std::cout << line << '\n';


	}


}



void FileReaderWriter::writeToFile()
{

	std::ofstream file;

	file.open(mFileName);
	file << "Player:\n";
	file << "Player Health:\n";
	file << gpGame->getDataKeeper()->getDataFile().playerHealth;
	file << "\n";
	file << "Player DamageRaduis\n";
	file << gpGame->getDataKeeper()->getDataFile().playerDamageRaduis;
	file << "\n";
	file << "Player Damge\n";
	file << gpGame->getDataKeeper()->getDataFile().playerDamage;
	file << "\n";
	file << "Player SpeedPercent\n";
	file << gpGame->getDataKeeper()->getDataFile().playerSpeedPercent;
	file << "\n";
	file << "Enemies:\n";
	file << "Enemy Health\n";
	file << gpGame->getDataKeeper()->getDataFile().enemyHealth;
	file << "\n";
	file << "Enemy DamageRaduis\n";
	file << gpGame->getDataKeeper()->getDataFile().enemyDamageRaduis;
	file << "\n";
	file << "Enemy Damage\n";
	file << gpGame->getDataKeeper()->getDataFile().enemyDamage;
	file << "\n";
	file << "Enemy SpeedPercent\n";
	file << gpGame->getDataKeeper()->getDataFile().enemySpeedPercent;
	file << "\n";
	file << "Enemy SpawnRate\n";
	file << gpGame->getDataKeeper()->getDataFile().enemySpawnRate;
	file << "\n";
	file << "Enemy SightRange\n";
	file << gpGame->getDataKeeper()->getDataFile().enemySightRange;
	file << "\n";
	file << "PowerUps:\n";
	file << "PowerUp SpawnRate\n";
	file << gpGame->getDataKeeper()->getDataFile().powerSpawnRate;
	file << "\n";
	file << "PowerUp CollostionRaduis\n";
	file << gpGame->getDataKeeper()->getDataFile().powerUpCollistionRadius;
	file << "\n";
	file << "PowerUp Speed\n";
	file << gpGame->getDataKeeper()->getDataFile().powerUpSpeed;
	file << "\n";
	file << "PowerUp Damage\n";
	file << gpGame->getDataKeeper()->getDataFile().powerUpDamage;
	file << "\n";
	file << "PowerUp Health\n";
	file << gpGame->getDataKeeper()->getDataFile().powerUpHealth;
	file << "\n";
	file.close();

}



