#pragma once

#include <Trackable.h>
#include "Steering.h"
#include "ArriveStreering.h"


class FaceArriveSteering : public Steering
{
	

public:
	FaceArriveSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID = INVALID_UNIT_ID, bool shouldFlee = false);


	virtual Steering* getSteering();

protected:
	
};