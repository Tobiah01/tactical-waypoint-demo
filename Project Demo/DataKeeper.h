#pragma once

#include "Trackable.h"
#include "PerformanceTracker.h"
#include "Defines.h"
#include "PlayerMoveToMessage.h"
#include "GameMessageManager.h"
#include <string>

struct DataStruct
{
	float playerHealth;
	float playerDamageRaduis;
	float playerSpeedPercent;
	float playerDamage;
	
	float enemyHealth;
	float enemyDamageRaduis;
	float enemySpeedPercent;
	float enemyDamage;
	float enemySpawnRate;
	float enemySightRange;

	float powerSpawnRate;
	float powerUpCollistionRadius;
	float powerUpSpeed;
	float powerUpDamage;
	float powerUpHealth;


	
};


class DataKeeper :public Trackable
{

public:

	DataKeeper();
	~DataKeeper();

	DataStruct getDataFile();

	void cleanUp();
	void setPlayerHealth(float health);
	void setPlayerDamageRaduis(float damgeRaduis);
	void setPlayerSpeedPercent(float speedPercent);
	void setPlayerDamage(float playerDamage);

	void setEnemyHealth(float health);
	void setEnemyDamageRaduis(float damageRaduis);
	void setEnemySpeedPercent(float speedPercent);
	void setEnemyDamage(float damage);
	void setEnemySpawnRate(float spawnRate);
	void setEnemySightRange(float sightRange);

	void setPowerUpSpawnRate(float spawnRate);
	void setPowerUpCollistionRadius(float radius);
	void setPowerUpDamge(float damage);
	void setPowerUpSpeed(float speed);
	void setPowerUpHealth(float health);
	

	
protected:

	// vars
	DataStruct mDataFile;






};