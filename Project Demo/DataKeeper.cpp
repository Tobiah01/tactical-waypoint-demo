#include "DataKeeper.h"



DataKeeper::DataKeeper() 
{
	mDataFile.playerHealth = 0;
	mDataFile.playerDamageRaduis = 0;
	mDataFile.playerSpeedPercent = 0;
	mDataFile.playerDamage = 0;

	mDataFile.enemyHealth = 0;
	mDataFile.enemyDamageRaduis = 0;
	mDataFile.playerSpeedPercent = 0;
	mDataFile.playerDamage = 0;
	mDataFile.enemySpawnRate = 0;
	mDataFile.enemySightRange = 0;

	mDataFile.powerSpawnRate = 0;
	mDataFile.powerUpCollistionRadius = 0;
	mDataFile.powerUpSpeed = 0;
	mDataFile.powerUpDamage = 0;
	mDataFile.powerUpHealth = 0;

}

DataKeeper::~DataKeeper() 
{

	cleanUp();

}

DataStruct DataKeeper::getDataFile()
{
	return mDataFile;
}

void DataKeeper::cleanUp() 
{


}

void DataKeeper::setPlayerHealth(float health) 
{
	mDataFile.playerHealth = health;
}

void DataKeeper::setPlayerDamageRaduis(float damgeRaduis) 
{
	mDataFile.playerDamageRaduis = damgeRaduis;
}

void DataKeeper::setPlayerSpeedPercent(float speedPercent) 
{
	mDataFile.playerSpeedPercent = speedPercent;
}

void DataKeeper::setPlayerDamage(float playerDamage) 
{
	mDataFile.playerDamage = playerDamage;
}
	


void DataKeeper::setEnemyHealth(float health) 
{
	mDataFile.enemyHealth = health;
}

void DataKeeper::setEnemyDamageRaduis(float damageRaduis) 
{
	mDataFile.enemyDamageRaduis = damageRaduis;
}

void DataKeeper::setEnemySpeedPercent(float speedPercent) 
{
	mDataFile.enemySpeedPercent = speedPercent;
}

void DataKeeper::setEnemyDamage(float damage) 
{
	mDataFile.enemyDamage = damage;
}

void DataKeeper::setEnemySpawnRate(float spawnRate) 
{
	mDataFile.enemySpawnRate = spawnRate;
}

void DataKeeper::setEnemySightRange(float sightRange) 
{
	mDataFile.enemySightRange = sightRange;
}



void DataKeeper::setPowerUpSpawnRate(float spawnRate) 
{
	mDataFile.powerSpawnRate = spawnRate;
}

void DataKeeper::setPowerUpCollistionRadius(float radius) 
{
	mDataFile.powerUpCollistionRadius = radius;
}

void DataKeeper::setPowerUpDamge(float damage) 
{
	mDataFile.powerUpDamage = damage;
}

void DataKeeper::setPowerUpSpeed(float speed) 
{
	mDataFile.powerUpSpeed = speed;
}

void DataKeeper::setPowerUpHealth(float health) 
{
	mDataFile.powerUpHealth = health;
}
