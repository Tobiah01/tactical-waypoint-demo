#pragma once
#include "Game.h"
#include "Vector2D.h"
#include "Trackable.h"
#include <vector>

struct startBlendStruct
{
	Vector2D blendSpeed;
	float blendLinerWeight;
};

struct endBlendStruct
{

	Vector2D finalBlendSpeed;
};

endBlendStruct Blend(std::vector<startBlendStruct> blends);
