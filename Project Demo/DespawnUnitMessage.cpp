#include "Game.h"
#include "GameMessageManager.h"
#include "DespawnUnitMessage.h"
#include "UnitManager.h"


DespawnUnitMessage::DespawnUnitMessage()
	:GameMessage(DESPAWN_UNIT_MESSAGE)
{
}

DespawnUnitMessage::~DespawnUnitMessage()
{
}

void DespawnUnitMessage::process()
{
	gpGame->getUnitManager()->deleteRandomUnit();
}

