#pragma once
#include <Trackable.h>
#include "Steering.h"

class FaceSeekSteering : public Steering
{

	friend class WanderChaseSteering;

public:
	FaceSeekSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID = INVALID_UNIT_ID, bool shouldFlee = false);

protected:
	virtual Steering* getSteering();
};