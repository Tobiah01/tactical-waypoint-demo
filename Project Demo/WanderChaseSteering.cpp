#include <cassert>

#include "Steering.h"
#include "WanderChaseSteering.h"
#include "Game.h"
#include "UnitManager.h"
#include "Unit.h"
#include "WanderSteering.h"
#include "FaceSeekSteering.h"
#include "FaceArriveSteering.h"
#include "DataKeeper.h"






WanderChaseSteering::WanderChaseSteering(const UnitID& ownerID, const Vector2D& targetLoc, const UnitID& targetID)
	: Steering()
{

	setOwnerID(ownerID);
	setTargetID(targetID);
	setTargetLoc(targetLoc);
}

Steering* WanderChaseSteering::getSteering()
{
	int targetRad = 100;

	Vector2D diff;
	Vector2D playerDiff;
	Unit* pOwner = gpGame->getUnitManager()->getUnit(mOwnerID);
	PhysicsData data = pOwner->getPhysicsComponent()->getData();
	Unit* pPlayer = gpGame->getUnitManager()->getPlayerUnit();


	diff = mTargetLoc - pOwner->getPositionComponent()->getPosition();

	playerDiff = pPlayer->getPositionComponent()->getPosition() - pOwner->getPositionComponent()->getPosition();


	if (playerDiff.getLength() < targetRad)
	{
		FaceArriveSteering* pArriveFace = new FaceArriveSteering(mOwnerID, pPlayer->getPositionComponent()->getPosition(), false);

		pArriveFace->getSteering();

		data = pArriveFace->getData();

		delete pArriveFace;
	}
	else 
	{
		WanderSteering* pWander = new WanderSteering(mOwnerID, mTargetLoc);
		
		pWander->getSteering();

		data = pWander->getData();

		mTargetLoc.setX(pWander->getTargetLoc().getX());
		mTargetLoc.setY(pWander->getTargetLoc().getY());

		delete pWander;
	}

	
	this->mData = data;
	return this;
}